#!/bin/bash
################################################################################
# Copyright 2024 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

CONFIG_FILE="/etc/modalai/extrinsics.conf"
USER=$(whoami)

FILE_LOC="/usr/share/modalai/extrinsic_configs"


print_usage () {
	echo "copy a default extrinsic config file to the following location:"
	echo "$CONFIG_FILE"
	echo ""

	find "$FILE_LOC" -type f | sort | while read -r FILE; do
		# Strip the directory path and file extension
		FILENAME=$(basename "$FILE" | sed 's/\(.*\)\..*/\1/')
		echo "voxl-configure-extrinsics $FILENAME"
	done

	echo ""
	echo "voxl-configure-extrinsics custom /xx/yy/my_custom_file.conf"
	echo ""
	echo "show this help message:"
	echo "voxl-configure-extrinsics help"
	echo ""
}


set_file_and_exit () {

	if [ "$#" != "1" ]; then
		echo "set_file_and_exit expected 1 argument, please specify a file"
		exit 1
	fi

	if [ ! -f $1 ]; then
		echo "ERROR, file $1 does not exist"
		print_usage
		exit 1
	fi

	echo "wiping old extrinsic config file"
	rm -rf ${CONFIG_FILE}
	echo "copying ${1} to ${CONFIG_FILE}"
	cp -f $1 ${CONFIG_FILE}
	echo "done configuring extrinsics"
	echo "you can check what was set by running voxl-inspect-extrinsics --all"
	exit 0
}


################################################################################
## actual start of execution, handle optional arguments first
################################################################################

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Please run this script as root"
	exit 1
fi


## parse arguments
case $1 in
	"")
		echo "missing arg"
		print_usage
		exit 1
		;;
	"h"|"-h"|"help"|"--help")
		print_usage
		exit 0
		;;

	"custom")
		set_file_and_exit $2
		;;
	*)
		set_file_and_exit "$FILE_LOC/${1}.conf"
		exit 0
esac


# shouldn't get here
exit 1
