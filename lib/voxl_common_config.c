/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h> // for access()
#include <math.h>

#include <modal_json.h>
#include "voxl_common_config.h"

// #define DEBUG


#define EXTRINSIC_CONFIG_FILE_HEADER "\
/**\n\
 * Extrinsic Configuration File\n\
 * This file is used by voxl-vision-hub, voxl-qvio-server, and voxl-openvins-server\n\
 *\n\
 * This configuration file serves to describe the static relations (translation\n\
 * and rotation) between sensors and bodies on a drone. Mostly importantly it\n\
 * configures the camera-IMU extrinsic relation for use by VIO. However, the\n\
 * user may expand this file to store many more relations if they wish. By\n\
 * consolidating these relations in one file, multiple processes that need this\n\
 * data can all be configured by this one configuration file. Also, copies of\n\
 * this file may be saved which describe particular drone platforms. The\n\
 * defaults describe the VOXL M500 drone reference platform.\n\
 *\n\
 * The file is constructed as an array of multiple extrinsic entries, each\n\
 * describing the relation from one parent to one child. Nothing stops you from\n\
 * having duplicates but this is not advised.\n\
 *\n\
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in\n\
 * intrinsic XYZ order in units of degrees. This corresponds to the parent\n\
 * rolling about its X axis, followed by pitching about its new Y axis, and\n\
 * finally yawing around its new Z axis to end up aligned with the child\n\
 * coordinate frame.\n\
 *\n\
 * The helper read function will read out and populate the associated data\n\
 * struct in both Tait-Bryan and rotation matrix format so the calling process\n\
 * can use either. Helper functions are provided to convert back and forth\n\
 * between the two rotation formats.\n\
 *\n\
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for\n\
 * ease of use when doing camera-IMU extrinsic relations in the field. This is\n\
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by\n\
 * the rc_math library. However, since the camera Z axis points out the lens, it\n\
 * is helpful for the last step in the rotation sequence to rotate the camera\n\
 * about its lens after first rotating the IMU's coordinate frame to point in\n\
 * the right direction by Roll and Pitch.\n\
 *\n\
 * The following online rotation calculator is useful for experimenting with\n\
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/\n\
 *\n\
 * The Translation vector should represent the center of the child coordinate\n\
 * frame with respect to the parent coordinate frame in units of meters.\n\
 *\n\
 * The parent and child name strings should not be longer than 63 characters.\n\
 *\n\
 * The relation from Body to Ground is a special case where only the Z value is\n\
 * read by voxl-vision-px4 and voxl-qvio-server so that these services know the\n\
 * height of the drone's center of mass (and tracking camera) above the ground\n\
 * when the drone is sitting on its landing gear ready for takeoff.\n\
 *\n\
 **/\n"




void vcc_print_extrinsic_conf(vcc_extrinsic_t* t, int n)
{
	int i,j;

	for(i=0; i<n; i++){
		printf("#%d:\n",i);
		printf("    parent:                %s\n", t[i].parent);
		printf("    child:                 %s\n", t[i].child);
		printf("    T_child_wrt_parent:  ");
		for(j=0;j<3;j++) printf("%7.3f ",t[i].T_child_wrt_parent[j]);
		printf("\n    RPY_parent_to_child:");
		for(j=0;j<3;j++) printf("%6.1f  ",t[i].RPY_parent_to_child[j]);
		printf("\n    R_child_to_parent:   ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[0][j]);
		printf("\n                         ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[1][j]);
		printf("\n                         ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[2][j]);
		printf("\n");
	}
	return;
}


// this would be much shorter lib librc_math, but we hand write the math to
// avoid having a dependency
static int _invert_tf(vcc_extrinsic_t in, vcc_extrinsic_t* out)
{

	if(out==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// copy over parent/child in reverse
	strcpy(out->child, in.parent);
	strcpy(out->parent, in.child);

	// rotation matrix is tranpose
	out->R_child_to_parent[0][0] = in.R_child_to_parent[0][0];
	out->R_child_to_parent[0][1] = in.R_child_to_parent[1][0];
	out->R_child_to_parent[0][2] = in.R_child_to_parent[2][0];
	out->R_child_to_parent[1][0] = in.R_child_to_parent[0][1];
	out->R_child_to_parent[1][1] = in.R_child_to_parent[1][1];
	out->R_child_to_parent[1][2] = in.R_child_to_parent[2][1];
	out->R_child_to_parent[2][0] = in.R_child_to_parent[0][2];
	out->R_child_to_parent[2][1] = in.R_child_to_parent[1][2];
	out->R_child_to_parent[2][2] = in.R_child_to_parent[2][2];

	// multiply new rotation matrix by negative of old translation to get new translation
	for(int i=0; i<3; i++){
		out->T_child_wrt_parent[i] = 0.0;
		for(int j=0; j<3; j++){
			out->T_child_wrt_parent[i] -= out->R_child_to_parent[i][j] * in.T_child_wrt_parent[j];
		}
	}

	// populate tait-bryan angles too
	vcc_rotation_matrix_to_tait_bryan_xyz_degrees(out->R_child_to_parent, out->RPY_parent_to_child);

	return 0;
}

// here A is the grandparent, B is parent, C is child
static int _combine_tf(vcc_extrinsic_t AB, vcc_extrinsic_t BC, vcc_extrinsic_t* AC)
{
	int i,j,k;

	//sanity checks
	if(AC==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	memset(AC, 0, sizeof(vcc_extrinsic_t));
	strcpy(AC->parent, AB.parent);
	strcpy(AC->child, BC.child);

	// combined rotation matrix
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			for(k=0;k<3;k++){
				AC->R_child_to_parent[i][j] += AB.R_child_to_parent[i][k] * BC.R_child_to_parent[k][j];
			}
		}
	}

	// translation
	for(i=0;i<3;i++){
		AC->T_child_wrt_parent[i] = AB.T_child_wrt_parent[i];
		for(j=0;j<3;j++){
			AC->T_child_wrt_parent[i] += AB.R_child_to_parent[i][j] * BC.T_child_wrt_parent[j];
		}
	}

	// populate tait-bryan angles too
	vcc_rotation_matrix_to_tait_bryan_xyz_degrees(AC->R_child_to_parent, AC->RPY_parent_to_child);
	return 0;
}


int vcc_read_extrinsic_conf_file(const char* path, vcc_extrinsic_t* t, int* n, int max_t)
{
	// vars and defaults
	int i, m;

	//sanity checks
	if(t==NULL || n==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}
	if(max_t<2){
		fprintf(stderr, "ERROR in %s, maximum number of extrinsics must be >=2\n", __FUNCTION__);
		return -1;
	}

	// read the data in
	cJSON* parent = json_read_file(path);
	if(parent==NULL){
		fprintf(stderr, "ERROR in %s, missing extrinsics file %s\n", __FUNCTION__, path);
		fprintf(stderr, "Likely you need to run voxl-configure-extrinsics to make a new file\n");
		return -1;
	}

	// file structure is just one big array of config structures
	cJSON* array = json_fetch_array_and_add_if_missing(parent, "extrinsics", &m);
	if(m > max_t){
		fprintf(stderr, "ERROR found %d extrinsics in file but maximum number of tags is set to %d\n", m, max_t);
		return -1;
	}

	// Now actually parse the json data into our output array!!
	for(i=0; i<m; i++){
		cJSON* item = cJSON_GetArrayItem(array, i);
		json_fetch_string(		item, "parent",			t[i].parent, 64);
		json_fetch_string(		item, "child",			t[i].child,  64);
		json_fetch_fixed_vector(item,"T_child_wrt_parent",t[i].T_child_wrt_parent, 3);
		json_fetch_fixed_vector(item,"RPY_parent_to_child",t[i].RPY_parent_to_child, 3);
		// also write out the rotation matrix equivalent
		vcc_tait_bryan_xyz_degrees_to_rotation_matrix(t[i].RPY_parent_to_child, t[i].R_child_to_parent);
	}

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", path);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if necessary
	if(json_get_modified_flag()){
		printf("The JSON extrinsic data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(path, parent, EXTRINSIC_CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	*n = m;
	return 0;
}



// quiet internal function for quickly scanning an array for a matching extrinsic relation
static int _find_extrinsic_in_array(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out)
{
	// If we can find the correct transform already in the list just use that
	for(int i=0;i<n;i++){
		// check if parent and child match
		if(strcmp(t[i].parent, parent)==0 && strcmp(t[i].child, child)==0){
			// we are done, copy and return
			memcpy(out, &t[i], sizeof(vcc_extrinsic_t));
			#ifdef DEBUG
			printf("DEBUG: Found direct map from %s to %s\n", parent, child);
			#endif
			return 0;
		}
	}

	// rescan for inverse. Yes yes, I know this isn't optimum. We will do a
	// proper graph data structure for this one day.
	for(int i=0;i<n;i++){
		// check if parent and child match backwards and return the inverse
		if(strcmp(t[i].parent, child)==0 && strcmp(t[i].child, parent)==0){
			#ifdef DEBUG
			printf("DEBUG: Found inverse map from %s to %s\n", parent, child);
			#endif
			return _invert_tf(t[i], out);
		}
	}
	return -1;
}


// this is now deprecated, replaced by vcc_find_extrinsic and vcc_fetch_extrinsic
int vcc_find_extrinsic_in_array(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out)
{
	return _find_extrinsic_in_array(parent, child, t, n, out);
}


int vcc_find_extrinsic(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out)
{
	// quick check if we can find the transform or inverse in the list
	if(_find_extrinsic_in_array(parent, child, t, n, out)==0){
		return 0;
	}

	// otherwise we will scan through for an intermediary
	vcc_extrinsic_t i_to_child;
	char* intermediary = NULL;

	// first look for any relations matching our desired parent
	for(int i=0;i<n;i++){
		if(strcmp(t[i].parent, parent)==0){
			intermediary = t[i].child;
			if(_find_extrinsic_in_array(intermediary, child, t, n, &i_to_child)==0){
				#ifdef DEBUG
				printf("DEBUG: forward map from grandparent %s to child %s using %s as intermediary\n", parent, child, intermediary);
				#endif
				return _combine_tf(t[i], i_to_child, out);
			}
		}
	}

	// also check for the inverse
	for(int i=0;i<n;i++){
		if(strcmp(t[i].child, parent)==0){
			intermediary = t[i].parent;
			if(_find_extrinsic_in_array(intermediary, child, t, n, &i_to_child)==0){
				// success
				#ifdef DEBUG
				printf("DEBUG: backward map from %s to %s using %s as intermediary\n", parent, child, intermediary);
				#endif
				vcc_extrinsic_t parent_to_i;
				_invert_tf(t[i], &parent_to_i);
				return _combine_tf(parent_to_i, i_to_child, out);
			}
		}
	}

	#ifdef DEBUG
	printf("DEBUG: Failed to find map from %s to %s\n", parent, child);
	#endif

	return -1;
}

int vcc_fetch_extrinsic(const char* parent, const char* child, vcc_extrinsic_t* out)
{
	vcc_extrinsic_t t[VCC_MAX_EXTRINSICS_IN_CONFIG];
	int n;
	if(vcc_read_extrinsic_conf_file(VCC_EXTRINSICS_PATH, t, &n, VCC_MAX_EXTRINSICS_IN_CONFIG)){
		fprintf(stderr, "ERROR in %s, failed to read file\n", __FUNCTION__);
		fprintf(stderr, "Likely you need to run voxl-configure-extrinsics to make a new file\n");
		return -1;
	}
	if(vcc_find_extrinsic(parent, child, t, n, out)){
		fprintf(stderr, "ERROR in %s, failed to find desired extrinsic relation in file\n", __FUNCTION__);
		fprintf(stderr, "Likely you need to run voxl-configure-extrinsics to make a new file\n");
		return -1;
	}
	return 0;
}


int vcc_fetch_R_child_to_body(const char* child, double R_child_to_body[3][3])
{
	vcc_extrinsic_t t;
	if(vcc_fetch_extrinsic("body", child, &t)){
		fprintf(stderr, "ERROR in %s\n", __FUNCTION__);
		fprintf(stderr, "Likely you need to run voxl-configure-extrinsics to make a new file\n");
	}
	memcpy(R_child_to_body, t.R_child_to_parent, 9*sizeof(double));
	return 0;
}


void vcc_rotation_matrix_to_tait_bryan_xyz_degrees(double R[3][3], double tb[3])
{
	#ifndef RAD_TO_DEG
	#define RAD_TO_DEG (180.0/M_PI)
	#endif

	// this algo borrowed from three.js then fixed
	tb[1] = asin(R[0][2])*RAD_TO_DEG;
	if(fabs(R[0][2])<0.9999999){
		tb[0] = atan2(-R[1][2], R[2][2])*RAD_TO_DEG;
		tb[2] = atan2(-R[0][1], R[0][0])*RAD_TO_DEG;
	}
	else{
		// this section appears to be wrong in three.js, fixed here
		tb[2] = atan2( R[2][1], R[1][1])*RAD_TO_DEG;
		tb[0] = 0.0;
	}

	return;
}

// as far as I can tell this is correct
void vcc_tait_bryan_xyz_degrees_to_rotation_matrix(double tb[3], double R[3][3])
{
	#ifndef DEG_TO_RAD
	#define DEG_TO_RAD (M_PI/180.0)
	#endif

	const double cx = cos(tb[0]*DEG_TO_RAD);
	const double sx = sin(tb[0]*DEG_TO_RAD);
	const double cy = cos(tb[1]*DEG_TO_RAD);
	const double sy = sin(tb[1]*DEG_TO_RAD);
	const double cz = cos(tb[2]*DEG_TO_RAD);
	const double sz = sin(tb[2]*DEG_TO_RAD);
	const double cxcz = cx * cz;
	const double cxsz = cx * sz;
	const double sxcz = sx * cz;
	const double sxsz = sx * sz;

	R[0][0] =  cy * cz;
	R[0][1] = -cy * sz;
	R[0][2] =  sy;
	R[1][0] =  cxsz + sxcz * sy;
	R[1][1] =  cxcz - sxsz * sy;
	R[1][2] = -sx * cy;
	R[2][0] =  sxsz - cxcz * sy;
	R[2][1] =  sxcz + cxsz * sy;
	R[2][2] =  cx * cy;

	return;
}


int vcc_tait_bryan_intrinsic_degrees_to_rotation_vector(double tb_deg[3], float rv[3])
{
	if(tb_deg==NULL||rv==NULL){
		fprintf(stderr,"ERROR: in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// convert from degree to rad
	double tb_rad[3];
	tb_rad[0] = tb_deg[0]*M_PI/180.0;
	tb_rad[1] = tb_deg[1]*M_PI/180.0;
	tb_rad[2] = tb_deg[2]*M_PI/180.0;

	double c1 = cos(tb_rad[0]/2.0);
	double s1 = sin(tb_rad[0]/2.0);
	double c2 = cos(tb_rad[1]/2.0);
	double s2 = sin(tb_rad[1]/2.0);
	double c3 = cos(tb_rad[2]/2.0);
	double s3 = sin(tb_rad[2]/2.0);
	double c1c2 = c1*c2;
	double s1s2 = s1*s2;
	double s1c2 = s1*c2;
	double c1s2 = c1*s2;

	// XYZ
	double w = c1c2*c3 - s1s2*s3;
	double x = s1c2*c3 + c1s2*s3;
	double y = c1s2*c3 - s1c2*s3;
	double z = c1c2*s3 + s1s2*c3;

	double norm = x*x+y*y+z*z;
	if(norm < 0.0001){
		rv[0]=0.0f;
		rv[1]=0.0f;
		rv[2]=0.0f;
		return 0;
	}

	double angle = 2.0*acos(w);
	double scale = angle/sqrt(norm);
	rv[0] = x*scale;
	rv[1] = y*scale;
	rv[2] = z*scale;
	return 0;
}



int vcc_read_lens_cal_file(const char* file, vcc_lens_cal_t* lens_cal, int use_second_cam_if_stereo)
{
	char final_path[256];

	// Check if the file path starts with "/"
	if (file[0] != '/') {
		// Prepend "/data/modalai/" to the file path
		snprintf(final_path, sizeof(final_path), "/data/modalai/%s", file);
	} else {
		// If the file already starts with "/", just use it as is
		snprintf(final_path, sizeof(final_path), "%s", file);
	}

	// Check if the file exists
	if(access(final_path, F_OK) != 0) return -1;

	cJSON* json = json_from_yaml(final_path);
	if(json==NULL){
		return -1;
	}

	cJSON* M = NULL;
	cJSON* D = NULL;
	double Mdata[9];
	double Ddata[9];
	int len = 0;
	int mode = 0; // 0=failed, 1=mono, 2=stereo first, 3=stereo second
	int is_fisheye = 0;
	int n_coef;

	// first look for M which should be present for a mono camera
	// if it's missing, try M1 or M2 for stereo cam files
	M = cJSON_GetObjectItem(json, "M");
	if(M != NULL){
		mode = 1;
	}
	else{
		if(!use_second_cam_if_stereo){
			M = cJSON_GetObjectItem(json, "M1");
			if(M != NULL) mode = 2;
		}
		else{
			M = cJSON_GetObjectItem(json, "M2");
			if(M != NULL) mode = 3;
		}
	}
	if(mode==0){
		fprintf(stderr, "WARNING failed to find field 'M' in camera cal file\n");
		return -1;
	}

	// now grab the correct distortion vector object by mode
	if(mode==1)      D = json_fetch_object(json, "D");
	else if(mode==2) D = json_fetch_object(json, "D1");
	else             D = json_fetch_object(json, "D2");

	if(D==NULL){
		fprintf(stderr, "WARNING failed to find field 'D' in camera cal file\n");
		return -1;
	}

	// check if we are a fisheye lens or not
	char model[128];
	if(json_fetch_string(json, "distortion_model", model, 127)){
		fprintf(stderr, "WARNING failed to find distortion_model in camera cal file\n");
		return -1;
	}
	if(strcmp(model,"fisheye")==0){
		is_fisheye = 1;
		n_coef = 4;
	}
	else{
		n_coef = 5;
	}

	// pick the data out of each M and D objects
	if(json_fetch_dynamic_vector(M, "data", Mdata, &len, 9)){
		fprintf(stderr, "WARNING failed to find M 'data' in camera cal file\n");
		return -1;
	}
	if(len != 9){
		fprintf(stderr, "WARNING M 'data' field in camera cal file should be of length 9\n");
		return -1;
	}

	if(json_fetch_dynamic_vector(D, "data", Ddata, &len, 9)){
		fprintf(stderr, "WARNING failed to find D 'data' in camera cal file\n");
		return -1;
	}
	if(len < n_coef){
		fprintf(stderr, "WARNING D 'data' field in camera cal file should be of length >= %d\n", n_coef);
		return -1;
	}

	// pick out height and width
	int width, height;
	if(json_fetch_int(json, "width", &width)){
		fprintf(stderr, "WARNING failed to find width in camera cal file\n");
		return -1;
	}
	if(json_fetch_int(json, "height", &height)){
		fprintf(stderr, "WARNING failed to find height in camera cal file\n");
		return -1;
	}


	// all data read from file, apply it to the output struct
	lens_cal->width				= width;
	lens_cal->height			= height;
	lens_cal->fx				= Mdata[0];
	lens_cal->fy				= Mdata[4];
	lens_cal->cx				= Mdata[2];
	lens_cal->cy				= Mdata[5];
	lens_cal->n_coeffs			= n_coef;
	lens_cal->is_fisheye		= is_fisheye;
	memset(lens_cal->D, 0, sizeof(float)*VCC_MAX_DISTORTION_COEFS);
	for(int i=0; i<n_coef; i++){
		lens_cal->D[i] = Ddata[i];
	}

	cJSON_Delete(json);
	return 0;
}



void vcc_print_vio_cam_conf(vio_cam_t* t, int n)
{
	int i,j;

	for(i=0; i<n; i++){
		if(i>=1) printf("\n");
		printf("cam %d:\n",i);
		printf("    enable:                %d\n", t[i].enable);
		printf("    name:                  %s\n", t[i].name);
		printf("    pipe_for_preview:      %s\n", t[i].pipe_for_preview);
		printf("    pipe_for_tracking:     %s\n", t[i].pipe_for_tracking);
		printf("    is_occluded_on_ground: %d\n", t[i].is_occluded_on_ground);
		printf("    imu:                   %s\n", t[i].imu);
		printf("    is_extrinsic_present:  %d\n", t[i].is_extrinsic_present);

		if(t[i].is_extrinsic_present){
			printf("    extrinsic cam wrt imu:\n");
			printf("    parent:                %s\n", t[i].extrinsic.parent);
			printf("    child:                 %s\n", t[i].extrinsic.child);
			printf("    T_cam_wrt_imu:       ");
			for(j=0;j<3;j++) printf("%7.3f ",t[i].extrinsic.T_child_wrt_parent[j]);
			printf("\n    RPY_imu_to_cam:    ");
			for(j=0;j<3;j++) printf("%7.1f ",t[i].extrinsic.RPY_parent_to_child[j]);
			printf("\n    R_cam_to_imu:        ");
			for(j=0;j<3;j++) printf("%7.3f ", t[i].extrinsic.R_child_to_parent[0][j]);
			printf("\n                         ");
			for(j=0;j<3;j++) printf("%7.3f ", t[i].extrinsic.R_child_to_parent[1][j]);
			printf("\n                         ");
			for(j=0;j<3;j++) printf("%7.3f ", t[i].extrinsic.R_child_to_parent[2][j]);
			printf("\n");
		}

		printf("    cal file:              %s\n", t[i].cal_file);
		printf("    is_cal_present:        %d\n", t[i].is_cal_present);
		if(t[i].is_cal_present){
			printf("    cam cal intrinsics:\n");
			printf("    width:                 %d\n", t[i].cal.width);
			printf("    height:                %d\n", t[i].cal.height);
			printf("    fx:                    %0.3f\n", (double)t[i].cal.fx);
			printf("    fy:                    %0.3f\n", (double)t[i].cal.fy);
			printf("    cx:                    %0.3f\n", (double)t[i].cal.cx);
			printf("    cy:                    %0.3f\n", (double)t[i].cal.cy);
			printf("    n_coeffs:              %d\n", t[i].cal.n_coeffs);
			printf("    is_fisheye:            %d\n", t[i].cal.is_fisheye);
			printf("    D: ");
			for(j=0;j<t[i].cal.n_coeffs;j++) printf("%0.4f ", (double)t[i].cal.D[j]);
			printf("\n");
		}
	}
	return;
}


int vcc_read_vio_cam_conf_file(vio_cam_t* c, int n, int only_read_enabled)
{
	if(c == NULL){
		fprintf(stderr, "ERROR in %s received null pointer\n", __FUNCTION__);
		return -1;
	}
	if(n<1){
		fprintf(stderr, "ERROR in %s n must be >= 1\n", __FUNCTION__);
		return -1;
	}

	// get parent JSON for reading
	cJSON *parent = json_read_file(VCC_VIO_CAMS_PATH);
	if(parent == NULL){
		fprintf(stderr, "please use voxl-configure-vio-cams to create one\n");
		return -1;
	}

	int n_cams_in_json;
	cJSON* cam_array = json_fetch_array(parent, "cams", &n_cams_in_json);

	if(cam_array==NULL){
		fprintf(stderr, "ERROR: %s missing cams array\n", VCC_VIO_CAMS_PATH);
		fprintf(stderr, "please use voxl-configure-vio-cams to create one\n");
		return -1;
	}
	if(n_cams_in_json == 0){
		fprintf(stderr, "ERROR: %s has an empty cams array\n", VCC_VIO_CAMS_PATH);
		fprintf(stderr, "please use voxl-configure-vio-cams to create one\n");
		return -1;
	}


	// we will need to fetch extrinsic configs while parsing, so read the file now
	vcc_extrinsic_t all_extrinsics[VCC_MAX_EXTRINSICS_IN_CONFIG];
	int n_extrinsics_read;
	if(vcc_read_extrinsic_conf_file(VCC_EXTRINSICS_PATH, all_extrinsics, &n_extrinsics_read, VCC_MAX_EXTRINSICS_IN_CONFIG)){
		fprintf(stderr, "ERROR in %s failed to read extrinsics file\n", __FUNCTION__);
		return -1;
	}

	// now parse through the whole array, keep a separate index for output
	// array index since we may be skipping some disabled cameras
	int out_i = 0;
	for(int in_i = 0; in_i < n_cams_in_json; in_i++)
	{
		cJSON* cam = cJSON_GetArrayItem(cam_array, in_i);

		// this is the only field we want to validate exists correctly and quit
		// early if the file is malformed. Everything else is unlikely to cause
		// issues while parsing and we can just check at the end for parse errors.
		int is_enabled;
		if(json_fetch_bool(cam, "enable", &is_enabled)){
			fprintf(stderr, "ERROR in %s, cam #%d missing the enable field\n",  __FUNCTION__, in_i);
			cJSON_Delete(parent);
			return -1;
		}

		// skip disabled cameras if requested
		if(only_read_enabled && !is_enabled) continue;
		c[out_i].enable = is_enabled;

		// now parse easy stuff
		json_fetch_string(cam, "name", c[out_i].name, 64);
		json_fetch_string(cam, "pipe_for_preview",  c[out_i].pipe_for_preview,  64);
		json_fetch_string(cam, "pipe_for_tracking", c[out_i].pipe_for_tracking, 64);
		json_fetch_bool(cam,   "is_occluded_on_ground", &c[out_i].is_occluded_on_ground);
		json_fetch_string(cam, "imu", c[out_i].imu, 64);

		// fetch extrinsics for this cam and flag as missing if needed
		c[out_i].is_extrinsic_present = 1;
		if(_find_extrinsic_in_array(c[out_i].imu, c[out_i].name, all_extrinsics, n_extrinsics_read, &c[out_i].extrinsic)){
			fprintf(stderr, "WARNING: failed to find extrinsics from %s to %s\n", c[out_i].imu, c[out_i].name);
			fprintf(stderr, "you may need to run voxl-configure-extrinsics for your platform or fix the file\n");
			c[out_i].is_extrinsic_present = 0;
		}

		// fetch lens cal and flag as missing if needed
		json_fetch_string(cam, "cal_file", c[out_i].cal_file, 128);
		c[out_i].is_cal_present = 1;
		int use_second_cam_if_stereo = 0;
		if(vcc_read_lens_cal_file(c[out_i].cal_file, &c[out_i].cal, use_second_cam_if_stereo)){
			fprintf(stderr, "WARNING: cal file %s is missing or invalid\n", c[out_i].cal_file);
			fprintf(stderr, "run voxl-check-calibration to see what calibration files are missing\n");
			c[out_i].is_cal_present = 0;
		}

		// increment the output array counter and stop parsing if we reached the end
		out_i++;
		if(out_i>=n) break;
	}

	// the above read functions will have set a common flag if there was an issue
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse vio cams config file\n");
		cJSON_Delete(parent);
		return -1;
	}


	// write modified data to disk if neccessary. Currently none of the above functions
	// modify the file but when we add new fields in the future this will help populate
	// those fields in old files
	if(json_get_modified_flag()){
		printf("The vio cams config file was modified during parsing, saving the changes to disk\n");
		json_write_to_file(VCC_VIO_CAMS_PATH, parent);
	}

	// all done parsing, free memory
	cJSON_Delete(parent);

	return out_i;
}
