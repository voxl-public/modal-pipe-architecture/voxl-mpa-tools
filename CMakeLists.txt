cmake_minimum_required(VERSION 3.3)
project(voxl-mpa-tools)

# James' standard list of cmake flags
set(CMAKE_C_FLAGS "-std=gnu99 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion -Wmissing-prototypes \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_C_FLAGS}")

set(CMAKE_CXX_FLAGS "-std=gnu++11 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_CXX_FLAGS}")


# find libraries
message( STATUS "processor: " ${CMAKE_SYSTEM_PROCESSOR} )
find_library(LIBRC_MATH_SO NAMES librc_math.so HINTS /usr/lib /usr/lib64 )
find_library(LIBMODAL_PIPE_SO NAMES libmodal_pipe.so HINTS /usr/lib /usr/lib64 )
find_library(LIBMODAL_JSON_SO NAMES libmodal_json.so HINTS /usr/lib /usr/lib64 )
find_library(LIBJPEG_TURBO_SO NAMES libturbojpeg.so HINTS /usr/lib /usr/lib64 )

message( STATUS "librc_math: " ${LIBRC_MATH_SO} )
message( STATUS "libmodal_pipe: " ${LIBMODAL_PIPE_SO} )
message( STATUS "libmodal_json: " ${LIBMODAL_JSON_SO} )
message( STATUS "libturbojpeg.so: " ${LIBJPEG_TURBO_SO} )

file(GLOB OpenCV_LIBS "/usr/lib64/libopencv*.so")


# for VOXL, install 64-bit libraries to lib64, 32-bit libs go in /usr/lib
if(CMAKE_SYSTEM_PROCESSOR MATCHES "^aarch64" OR CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
	set(LIB_INSTALL_DIR /usr/lib64)
	# build 64-bit tools and 64-bit lib
	add_subdirectory(tools)
	add_subdirectory(lib)
else()
	set(LIB_INSTALL_DIR /usr/lib)
	# build 32-bit lib, don't worry baout 32-bit tools
	add_subdirectory(lib)
endif()


# add targets from subdirectories

