/**
 * Extrinsic Configuration File
 * This file is used by voxl-vision-px4, voxl-qvio-server, and voxl-vins-server.
 *
 * This configuration file serves to describe the static relations (translation
 * and rotation) between sensors and bodies on a drone. Mostly importantly it
 * configures the camera-IMU extrinsic relation for use by VIO. However, the
 * user may expand this file to store many more relations if they wish. By
 * consolidating these relations in one file, multiple processes that need this
 * data can all be configured by this one configuration file. Also, copies of
 * this file may be saved which describe particular drone platforms. The
 * defaults describe the VOXL M500 drone reference platform.
 *
 * The file is constructed as an array of multiple extrinsic entries, each
 * describing the relation from one parent to one child. Nothing stops you from
 * having duplicates but this is not advised.
 *
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in
 * intrinsic XYZ order in units of degrees. This corresponds to the parent
 * rolling about its X axis, followed by pitching about its new Y axis, and
 * finally yawing around its new Z axis to end up aligned with the child
 * coordinate frame.
 *
 * The helper read function will read out and populate the associated data
 * struct in both Tait-Bryan and rotation matrix format so the calling process
 * can use either. Helper functions are provided to convert back and forth
 * between the two rotation formats.
 *
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for
 * ease of use when doing camera-IMU extrinsic relations in the field. This is
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by
 * the rc_math library. However, since the camera Z axis points out the lens, it
 * is helpful for the last step in the rotation sequence to rotate the camera
 * about its lens after first rotating the IMU's coordinate frame to point in
 * the right direction by Roll and Pitch.
 *
 * The following online rotation calculator is useful for experimenting with
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/
 *
 * The Translation vector should represent the center of the child coordinate
 * frame with respect to the parent coordinate frame in units of meters.
 *
 * The parent and child name strings should not be longer than 63 characters.
 *
 * The relation from Body to Ground is a special case where only the Z value is
 * read by voxl-vision-px4 and voxl-qvio-server so that these services know the
 * height of the drone's center of mass (and tracking camera) above the ground
 * when the drone is sitting on its landing gear ready for takeoff.
 *
 **/

/*
NOTE these are the camera relations from CAD
The values below were derived experimentally from QVIO


gen2 from CAD (30 deg out, 45 deg down)
		}, {
			"parent":	"imu_apps",
			"child":	"trackingL",
			"T_child_wrt_parent":	[-0.082, 0.0236, 0.0251],
			"RPY_parent_to_child":	[-26.6, -37.8, -129.3]
		}, {
			"parent":	"imu_apps",
			"child":	"trackingR",
			"T_child_wrt_parent":	[-0.082, -0.0116, 0.0251],
			"RPY_parent_to_child":	[26.6, -37.8, -50.7]
		}, {

gen2 as converged on YSERA Sept 29 2023
		}, {
			"parent":	"imu_apps",
			"child":	"trackingL",
			"T_child_wrt_parent":	[-0.082, 0.0236, 0.0251],
			"RPY_parent_to_child":	[-25.2,  -33.4, -126.0]
		}, {
			"parent":	"imu_apps",
			"child":	"trackingR",
			"T_child_wrt_parent":	[-0.082, -0.0116, 0.0251],
			"RPY_parent_to_child":	[24.6,  -33.8,  -50.3]
		}, {

gen3 as converged on a production unit Dec 20 2023
		}, {
			"parent":	"imu_apps",
			"child":	"trackingL",
			"T_child_wrt_parent":	[-0.082, 0.0236, 0.0251],
			"RPY_parent_to_child":	[-23.6,  -38.4, -124.6]
		}, {
			"parent":	"imu_apps",
			"child":	"trackingR",
			"T_child_wrt_parent":	[-0.082, -0.0116, 0.0251],
			"RPY_parent_to_child":	[24.9,  -38.2,  -57.1]
		}, {

*/

{
	"name":	"FPV_revB",
	"extrinsics":	[{
			"parent":	"body",
			"child":	"imu_apps",
			"T_child_wrt_parent":	[-0.0235, 0.006, 0.009],
			"RPY_parent_to_child":	[0, 0, 180]
		}, {
			"parent":	"imu_apps",
			"child":	"trackingL",
			"T_child_wrt_parent":	[-0.082, 0.0236, 0.0251],
			"RPY_parent_to_child":	[-23.6,  -38.4, -124.6]
		}, {
			"parent":	"imu_apps",
			"child":	"trackingR",
			"T_child_wrt_parent":	[-0.082, -0.0116, 0.0251],
			"RPY_parent_to_child":	[24.9,  -38.2,  -57.1]
		}, {
			"parent":	"body",
			"child":	"lepton0_raw",
			"T_child_wrt_parent":	[-0.06, 0, 0.01],
			"RPY_parent_to_child":	[0, 0, 90]
		}, {
			"parent":	"body",
			"child":	"lepton0_16raw",
			"T_child_wrt_parent":	[-0.06, 0, 0.01],
			"RPY_parent_to_child":	[0, 0, 90]
		}, {
			"parent":	"body",
			"child":	"ground",
			"T_child_wrt_parent":	[0, 0, 0.025],
			"RPY_parent_to_child":	[0, 0, 0]
		}]
}
