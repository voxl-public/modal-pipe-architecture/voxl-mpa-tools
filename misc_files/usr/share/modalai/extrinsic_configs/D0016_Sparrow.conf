/**
 * The file is constructed as an array of multiple extrinsic entries, each
 * describing the relation from one parent to one child. Nothing stops you from
 * having duplicates but this is not advised.
 *
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in
 * intrinsic XYZ order in units of degrees. This corresponds to the parent
 * rolling about its X axis, followed by pitching about its new Y axis, and
 * finally yawing around its new Z axis to end up aligned with the child
 * coordinate frame.
 *
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for
 * ease of use when doing camera-IMU extrinsic relations in the field. This is
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by
 * the rc_math library. However, since the camera Z axis points out the lens, it
 * is helpful for the last step in the rotation sequence to rotate the camera
 * about its lens after first rotating the IMU's coordinate frame to point in
 * the right direction by Roll and Pitch.
 *
 * The Translation vector should represent the center of the child coordinate
 * frame with respect to the parent coordinate frame in units of meters.
 *
 * The parent and child name strings should not be longer than 63 characters.
 *
 * The relation from Body to Ground is a special case where only the Z value is
 * read by voxl-vision-hub and voxl-qvio-server so that these services know the
 * height of the drone's center of mass (and tracking camera) above the ground
 * when the drone is sitting on its landing gear ready for takeoff.
 *
 * See https://docs.modalai.com/configure-extrinsics/ for more details.
 **/

{
	"name":	"D0016_Sparrow",
	"extrinsics": [{
			"parent": "imu_apps",
			"child":  "tracking_front",
			"T_child_wrt_parent": [-0.012, 0.0571, 0.0082],
			"RPY_parent_to_child":    [-80, 0, 180]
		}, {
			"parent": "imu_apps",
			"child":  "tracking_down",
			"T_child_wrt_parent": [-0.012, -0.0019, 0.0244],
			"RPY_parent_to_child":    [0, 0, 180]
		}, {
			"parent": "imu_apps",
			"child":  "tracking_rear",
			"T_child_wrt_parent": [-0.012, -0.0187, 0.0082],
			"RPY_parent_to_child":    [80, 0, 0]
		}, {
			"parent":	"imu_apps",
			"child":	"lepton_front",
			"T_child_wrt_parent":	[-0.012, 0.0428, 0.021],
			"RPY_parent_to_child":	[-75, 0, 180]
		}, {
			"parent":	"imu_apps",
			"child":	"lepton_down",
			"T_child_wrt_parent":	[-0.012, 0.0155, 0.0265],
			"RPY_parent_to_child":	[0, 0, 180]
		}, {
			"parent": "body",
			"child":  "imu_apps",
			"T_child_wrt_parent": [-0.0192, -0.012, -0.005],
			"RPY_parent_to_child":    [0, 0, -90]
		}, {
			"parent": "body",
			"child":  "imu_px4",
			"T_child_wrt_parent": [-0.0006, -0.010, -0.005],
			"RPY_parent_to_child":    [0, 0, -90]
		}, {
			"parent": "body",
			"child":  "ground",
			"T_child_wrt_parent": [0, 0, 0.030],
			"RPY_parent_to_child":    [0, 0, 0]
		}]
}
