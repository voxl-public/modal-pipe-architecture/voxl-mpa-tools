/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <voxl_common_config.h>

#define MODE_NONE   0
#define MODE_QUIET  1
#define MODE_ALL    2
#define MODE_SINGLE 3

static int mode = MODE_NONE;
static char parent[64];
static char child[64];

static void _print_usage(void)
{
	printf("\n\
Tool to print extrinsic config as loaded from disk.\n\
\n\
quiet mode:  only validate the file contents and exit\n\
all mode:    print all extrisic relations\n\
single mode: print the relation between a specified parent and child\n\
\n\
\n\
-h, --help     print this help message\n\
-q, --quiet    quiet mode\n\
-a, --all      print all relations\n\
-p, --parent   Specify the parent in single mode\n\
-c, --child    Specify the child in single mode\n\
\n\
typical usage:\n\
/# voxl-inspect-extrinsics --all\n\
/# voxl-inspect-extrinsics -p body -c tracking_front\n\
\n");
	return;
}



static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{"quiet",			no_argument,		0,	'q'},
		{"all",				no_argument,		0,	'a'},
		{"parent",			required_argument,	0,	'p'},
		{"child",			required_argument,	0,	'c'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "hqap:c:", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'h':
			_print_usage();
			exit(0);

		case 'q':
			if(mode){
				fprintf(stderr, "INVALID ARGS, please only specify one mode\n");
				_print_usage();
				exit(-1);
			}
			mode = MODE_QUIET;
			break;

		case 'a':
			if(mode){
				fprintf(stderr, "INVALID ARGS, please only specify one mode\n");
				_print_usage();
				exit(-1);
			}
			mode = MODE_ALL;
			break;

		case 'p':
			if(mode==MODE_ALL || mode==MODE_QUIET){
				fprintf(stderr, "INVALID ARGS, please only specify one mode\n");
				_print_usage();
				exit(-1);
			}
			mode = MODE_SINGLE;
			strcpy(parent, optarg);
			break;

		case 'c':
			if(mode==MODE_ALL || mode==MODE_QUIET){
				fprintf(stderr, "INVALID ARGS, please only specify one mode\n");
				_print_usage();
				exit(-1);
			}
			mode = MODE_SINGLE;
			strcpy(child, optarg);
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	if(mode == MODE_NONE){
		fprintf(stderr, "INSUFFICIENT ARGS, please specify one mode\n");
		_print_usage();
		exit(-1);
	}

	if(mode == MODE_SINGLE){
		if(parent[0]==0){
			fprintf(stderr, "ERROR: have child frame but missing parent frame\n");
			exit(-1);
		}
		if(child[0]==0){
			fprintf(stderr, "ERROR: have parent frame but missing child frame\n");
			exit(-1);
		}
	}

	return 0;
}


int main(int argc, char* argv[])
{
	if(parse_opts(argc, argv)) return -1;

	// all and quiet modes read the whole file
	if(mode==MODE_ALL || mode==MODE_QUIET){

		int n;
		vcc_extrinsic_t t[VCC_MAX_EXTRINSICS_IN_CONFIG];

		if(vcc_read_extrinsic_conf_file(VCC_EXTRINSICS_PATH, t, &n, VCC_MAX_EXTRINSICS_IN_CONFIG)){
			return -1;
		}

		// sometimes we run this in quiet mode just to parse and validate
		if(mode == MODE_QUIET) return 0;

		// print the name field if available
		cJSON* parent = json_read_file(VCC_EXTRINSICS_PATH);
		if(parent==NULL) return -1;

		if(cJSON_HasObjectItem(parent, "name")){
			char name[128];
			int ret = json_fetch_string(parent, "name", name, 128);
			if(!ret){
				printf("name: %s\n", name);
			}
		}

		// print the table of values with our helper
		vcc_print_extrinsic_conf(t, n);
		return 0;
	}

	// If we got here we are in single mode
	vcc_extrinsic_t t;
	if(vcc_fetch_extrinsic(parent, child, &t)) return -1;
	vcc_print_extrinsic_conf(&t, 1);

	return 0;
}
