/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <c_library_v2/common/mavlink.h>
#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

// copied from voxl-vision-hub.h to avoid circular dependency since
// voxl-vision-hub also is dependent on voxl-mpa-tools
#define GPS_RAW_OUT_PATH	(MODAL_PIPE_DEFAULT_BASE_DIR "mavlink_gps_raw_int/")
#define CLIENT_NAME			"voxl-inspect-gps"

static int newline = 0;


static void _print_usage(void)
{
	printf("\n\
Tool to print GPS data from autopilot to the screen for inspection.\n\
If no data comes through then either:\n\
    1. the voxl-vision-hub service isn't running\n\
    2. Autopilot isn't connected\n\
    3. Autopilot doesn't have GPS lock yet\n\
\n\
-h, --help              print this help message\n\
-n, --newline           print newline between each sample\n\
\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{"newline",			no_argument,		0,	'n'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "hn", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 'n':
			newline = 1;
			break;
		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}



// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL DISABLE_WRAP FONT_BOLD);
	printf(" dt(ms) |");
	printf("fix type|");
	printf("Sats|");
	printf("Latitude(deg)|");
	printf("Longitude(deg)|");
	printf(" Alt(m) |");
	printf("Vel(m/s)|");
	printf("HorErr(m)|");
	printf("AltErr(m)|\n");
	printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// callback for simple helper when data is ready
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL){
		return;
	}

	// grab the first one
	mavlink_message_t* msg = &msg_array[0];

	// fetch integer values from the unpacked mavlink message
	uint64_t tnow = mavlink_msg_gps_raw_int_get_time_usec(msg);
	int32_t  lat  = mavlink_msg_gps_raw_int_get_lat(msg);
	int32_t  lon  = mavlink_msg_gps_raw_int_get_lon(msg);
	int32_t  alt  = mavlink_msg_gps_raw_int_get_alt(msg);
	uint32_t vel  = mavlink_msg_gps_raw_int_get_vel(msg);
	uint8_t  fix  = mavlink_msg_gps_raw_int_get_fix_type(msg);
	uint8_t  sat  = mavlink_msg_gps_raw_int_get_satellites_visible(msg);
	uint32_t hacc = mavlink_msg_gps_raw_int_get_h_acc(msg);
	uint32_t vacc = mavlink_msg_gps_raw_int_get_v_acc(msg);

	// convert integer values to more useful units
	double lat_deg = (double)lat/10000000.0;
	double lon_deg = (double)lon/10000000.0;
	double alt_m   = (double)alt/1000.0;
	double vel_ms  = (double)vel/100.0;
	double hacc_m  = (double)hacc/1000.0;
	double vacc_m  = (double)vacc/1000.0;

	// keep track of time since last message dt
	static uint64_t t_last_us = 0;
	double dt_ms;
	if(t_last_us==0){
		dt_ms = 0.0;
	}
	else{
		dt_ms = (double)(tnow-t_last_us)/1000.0;
	}
	t_last_us = tnow;

	// construct a string to print from the fix type
	char fix_string[16];
	switch(fix){
		case 0:
			sprintf(fix_string," NO GPS ");
			break;
		case 1:
			sprintf(fix_string," NO FIX ");
			break;
		case 2:
			sprintf(fix_string," 2D FIX ");
			break;
		case 3:
			sprintf(fix_string," 3D FIX ");
			break;
		case 4:
			sprintf(fix_string,"  DGPS  ");
			break;
		case 5:
			sprintf(fix_string,"RTKFLOAT");
			break;
		case 6:
			sprintf(fix_string,"RTKFIXED");
			break;
		case 7:
			sprintf(fix_string," STATIC ");
			break;
		case 8:
			sprintf(fix_string," 3D PPP ");
			break;
		default:
			sprintf(fix_string," UNKNOWN");
			break;
	}

	// print everything in one go.
	if(!newline) printf("\r");
	printf("%7.1f |%s|%3d |%12.7f | %12.7f | %6.1f | %6.2f | %7.3f | %7.3f |",\
	dt_ms, fix_string, sat, lat_deg, lon_deg, alt_m, vel_ms, hacc_m, vacc_m);

	// only print to a new line if requested
	if(newline) printf("\n");
	fflush(stdout);

	return;
}


int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// normal non-test mode
	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for Autopilot %s\n", GPS_RAW_OUT_PATH);
	int ret = pipe_client_open(0, GPS_RAW_OUT_PATH, CLIENT_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
								MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	if(ret){
		fprintf(stderr, "ERROR: failed to open pipe %s\n", GPS_RAW_OUT_PATH);
		pipe_print_error(ret);
		fprintf(stderr, "Probably voxl-vision-hub is not running\n");
		return -1;
	}

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}
