/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>

#include <rc_math.h>

#include "common.h"

#define CLIENT_NAME			"voxl-integrate-gyro-3d"
#define RAD_TO_DEG			(180.0/M_PI)
#define BUF_SIZE			2000
#define SAMPLE_TIME_S		0.5


static int en_debug = 0;
static int64_t last_ts_inserted = 0;
static int64_t last_ts_integrated = 0;

static rc_timed3_ringbuf_t buf = RC_TIMED3_RINGBUF_INITIALIZER;
static rc_matrix_t R_since_start = RC_MATRIX_INITIALIZER;
static rc_matrix_t R_last_segment = RC_MATRIX_INITIALIZER;
static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];


static void _print_usage(void)
{
	printf("\n\
This is a demonstration of how to use the timed3-ringbuf module in librc_math\n\
for measuring body and camera rotation with the gyro between camera frames.\n\
\n\
-d, --debug                 enable extra debug prints\n\
-h, --help                  print this help message\n\
\n\
example usage on VOXL:\n\
/# voxl-inspect-imu imu1\n\
/# voxl-inspect-imu imu0\n\
\n\
example usage on VOXL2:\n\
/# voxl-inspect-imu imu_apps\n\
\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",			no_argument,		0,	'd'},
		{"help",			no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "dh", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'd':
			en_debug = 1;
			break;
		case 'h':
			_print_usage();
			exit(0);
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL RESET_FONT);
	printf("\n");
	printf(FONT_BOLD);
	printf("from start: Roll  Pitch  Yaw    segment: Roll  Pitch  Yaw   calc_us\n");
	printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// callback to put new imu data into the buffer
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	imu_data_t* d = pipe_validate_imu_data_t(data, bytes, &n_packets);

	// insert each gyro sample to our timed3 buffer
	for(int i=0;i<n_packets;i++){
		double val[3];
		val[0] = (double)d[i].gyro_rad[0];
		val[1] = (double)d[i].gyro_rad[1];
		val[2] = (double)d[i].gyro_rad[2];
		rc_timed3_ringbuf_insert(&buf, d[i].timestamp_ns, val);
		last_ts_inserted = d[i].timestamp_ns;
	}
	return;
}




int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// start rotation at 0
	rc_matrix_identity(&R_since_start, 3);
	rc_timed3_ringbuf_alloc(&buf, BUF_SIZE);

	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);


	int flags = CLIENT_FLAG_EN_SIMPLE_HELPER;

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	int ret = pipe_client_open(0, pipe_path, CLIENT_NAME, \
				flags, IMU_RECOMMENDED_READ_BUF_SIZE);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_print_error(ret);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running){
		usleep(SAMPLE_TIME_S * 1000000);

		// nothing to do if not yet connected
		if(!pipe_client_is_connected(0)) continue;

		int64_t t_now   = _time_monotonic_ns();
		int64_t t_end   = last_ts_inserted;
		int64_t t_start = last_ts_integrated;

		// check for data too old
		if((t_now-t_end) > 1000000000){
			if(en_debug) fprintf(stderr, "gyro data in buffer too old\n");
			continue;
		}

		// if this is our first pass, set start time as the oldest data we have
		int64_t oldest_ts_in_buf;
		rc_timed3_ringbuf_get_ts_at_pos(&buf, buf.items_in_buf-1, &oldest_ts_in_buf);

		if(t_start == 0 || t_start<oldest_ts_in_buf){
			if(en_debug) fprintf(stderr, "setting new start point to oldest data\n");
			t_start = oldest_ts_in_buf;
		}

		// buffer stopped filling, wait to reconnect
		if(t_start>=t_end) continue;

		if(en_debug){
			printf("t_start is %5dms ago, t_end is %5dms ago\n",\
													(int)((t_now-t_start)/1000000),\
													(int)((t_now-t_end)/1000000));
		}

		int64_t t_b4   = _time_monotonic_ns();
		int ret = rc_timed3_ringbuf_integrate_gyro_3d(&buf, t_start, t_end, &R_last_segment);
		int64_t t_after   = _time_monotonic_ns();
		if(ret){
			fprintf(stderr, "integrate gyro returned %d\n", ret);
			continue;
		}



		// add last second's rotation to the total
		rc_matrix_right_multiply_inplace(&R_since_start, R_last_segment);

		double tb_all[3], tb_last_s[3];
		rc_rotation_to_tait_bryan(R_since_start, &tb_all[0], &tb_all[1], &tb_all[2]);
		tb_all[0] *= RAD_TO_DEG;
		tb_all[1] *= RAD_TO_DEG;
		tb_all[2] *= RAD_TO_DEG;

		rc_rotation_to_tait_bryan(R_last_segment, &tb_last_s[0], &tb_last_s[1], &tb_last_s[2]);
		tb_last_s[0] *= RAD_TO_DEG;
		tb_last_s[1] *= RAD_TO_DEG;
		tb_last_s[2] *= RAD_TO_DEG;

		if(!en_debug) printf("\r");
		printf("          %6.1f %6.1f %6.1f       %6.1f %6.1f %6.1f   %5d", \
									tb_all[0],tb_all[1],tb_all[2],\
									tb_last_s[0],tb_last_s[1],tb_last_s[2],\
									(int)(t_after - t_b4)/1000);
		if(en_debug) printf("\n");
		fflush(stdout);

		last_ts_integrated = t_end;

	}

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT);
	pipe_client_close_all();

	return 0;
}
