/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h> // for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>
#include <list>
#include <vector>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

#define CLIENT_NAME     "voxl-check-camera-repetition"

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];

static unsigned int hist_size = 10;
static unsigned int compare_size = 1024; //640*480;
std::list<std::vector<char> > frame_history;

static void _print_usage(void) {
    printf("\n\
\n\
Validate camera data from Modal Pipe Architecture.\n\
\n\
Options are:\n\
-c, --compare-size {n}   number of bytes to compare (default 1024)\n\
-h, --help               print this help message\n\
-s, --history-size {n}   number of recent frames to check against (default 10)\n\
\n\
typical usage:\n\
/# voxl-check-camera-repetition tracking -c 2048 -s 15\n\
\n");
    return;
}



static int _parse_opts(int argc, char* argv[])
{
    static struct option long_options[] =
    {
        {"compare-size",       required_argument,  0, 'c'},
        {"help",               no_argument,        0, 'h'},
        {"history-size",       required_argument,  0, 's'},
        {0, 0, 0, 0}
    };

    while(1){
        int option_index = 0;
        int c = getopt_long(argc, argv, "c:hs:", long_options, &option_index);

        if(c == -1) break; // Detect the end of the options.

        switch(c){
        case 0:
            // for long args without short equivalent that just set a flag
            // nothing left to do so just break.
            if (long_options[option_index].flag != 0) break;
            break;
            break;
        case 'c':
            compare_size = atoi(optarg);
            break;
        case 'h':
            _print_usage();
            exit(0);
        case 's':
            hist_size = atoi(optarg);
            break;
        default:
            _print_usage();
            exit(0);
        }
    }

    // scan through the non-flagged arguments for the desired pipe
    for(int i=optind; i<argc; i++){
        if(pipe_path[0]!=0){
            fprintf(stderr, "ERROR: Please specify only one pipe\n");
            _print_usage();
            exit(-1);
        }
        if(pipe_expand_location_string(argv[i], pipe_path)<0){
            fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
            exit(-1);
        }
    }

    // make sure a pipe was given
    if(pipe_path[0] == 0){
        fprintf(stderr, "ERROR: You must specify a pipe name\n");
        _print_usage();
        exit(-1);
    }

    return 0;
}

static bool find_duplicate_frame(char * data)
{
    bool duplicate_found = false;
  
    int count = frame_history.size();
  
    for (auto old_frame : frame_history)
    {
        if (memcmp(&old_frame[0],data,compare_size)==0)
        {
          //printf("found duplicate frame!!!\n");
          duplicate_found = true;
          printf("\nFound duplicate %d frames back\n\n", count);
          break;
        }
        count--;
    }
    std::vector<char> v;
    v.resize(compare_size);
    memcpy(&v[0],data,compare_size);
    frame_history.push_back(v);
    while (frame_history.size()>hist_size)
        frame_history.pop_front();
  
    return duplicate_found;
}

// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{

    return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
    return;
}


// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused))int ch,
                       __attribute__((unused))camera_image_metadata_t meta, 
                                              char* frame, 
                       __attribute__((unused))void* context)
{

    // prints can be quite long, disable terminal wrapping
    printf(DISABLE_WRAP);
    static int numNot = 0;

    if(find_duplicate_frame(frame)){
        numNot = 0;

    } else {

        printf("\rNo duplicates found in %d frames", ++numNot); 

    }

    return;
}


int main(int argc, char* argv[])
{
    // check for options
    if(_parse_opts(argc, argv)) return -1;


    printf("Using compare size of %d bytes\n", compare_size);
    printf("Using history of %d frames\n\n", hist_size);

    // set some basic signal handling for safe shutdown.
    // quitting without cleanup up the pipe can result in the pipe staying
    // open and overflowing, so always cleanup properly!!!
    enable_signal_handler();
    main_running = 1;
    
    // normal non-test mode
    // set up all our MPA callbacks
    pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
    pipe_client_set_connect_cb(0, _connect_cb, NULL);
    pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

    // request a new pipe from the server
    int ret = pipe_client_open(0, pipe_path, CLIENT_NAME, \
                EN_PIPE_CLIENT_CAMERA_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, 0);

    // check for errors trying to connect to the server pipe
    if(ret<0){
        pipe_print_error(ret);
        printf(ENABLE_WRAP);
        return -1;
    }

    // keep going until the  signal handler sets the running flag to 0
    while(main_running) usleep(500000);

    // all done, signal pipe read threads to stop
    printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
    pipe_client_close_all();

    return 0;
}
