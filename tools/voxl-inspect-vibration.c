/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <time.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <rc_math.h>

#include "common.h"

#define CLIENT_NAME			"voxl-inspect-vibration"
#define SAMPLE_RATE			1000 // TODO adjust based on actual sample rate

// lower-bounds on what we consider medium and high noise levels
#define YLW_BOUND_ACCL	2.0
#define RED_BOUND_ACCL	5.0
#define YLW_BOUND_GYRO	0.2
#define RED_BOUND_GYRO	0.5

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN] = "imu_apps";
static int pipe_set = 0;
static int n_samples = 1500;		// samples to read each calculation
static double cutoff_freq = 50.0;
static rc_filter_t filters[6];
static rc_vector_t vectors[6];
static double max[6];
static int newline = 0;
static int timeout = 0;

static void _print_usage(void)
{
	printf("\n\
Tool to measure IMU vibration through Modal Pipe Architecture.\n\
This works by calculating the RMS of each accelerometer and gyro channel\n\
over n_samples (default 1500). This can also be interpreted as the standard\n\
deviation of the signal.\n\
\n\
The data is pre-filtered through a configurable butterworth high-pass filter\n\
with default cutoff frequency of 50hz so that steady desirable motion does not\n\
contribute to the vibration measurement. Change this frequency with the --freq\n\
argument. Set it to 0 to disable the filter altogether.\n\
\n\
-f, --freq {freq_hz}      high-pass cutoff frequency (hz)\n\
-h, --help                print this help message\n\
-l, --newline             print new line for every sample\n\
-n, --n_samples {samples} number of samples to use for each calculation\n\
-t, --timeout             return after printing the first measurement\n\
\n\
typical usage on VOXL2:\n\
/# voxl-inspect-vibration\n\
\n\
typical usage on VOXL 1:\n\
/# voxl-inspect-vibration imu_1\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"freq",            required_argument, 0, 'f'},
		{"help",            no_argument,       0, 'h'},
		{"newline",         no_argument,       0, 'l'},
		{"n_samples",       required_argument, 0, 'n'},
		{"timeout",         no_argument,       0, 't'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "f:hln:t", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if(long_options[option_index].flag != 0) break;
			break;
		case 'f':
			cutoff_freq = atof(optarg);
			if(cutoff_freq<0.0){
				cutoff_freq = 0.0;
			}
			break;
		case 'h':		
			_print_usage();
			exit(0);
		case 'l':
			newline = 1;
			break;
		case 'n':
			n_samples = atoi(optarg);
			if(n_samples<2){
				fprintf(stderr, "ERROR: n_samples must be >=2, recommended >50\n");
				exit(-1);
			}
			break;
		case 't':
			timeout = 1;
			newline = 1;
			break;
		default:
			_print_usage();
			exit(-1);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0 && pipe_set){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
		pipe_set = 1;
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{	
	if(!timeout){
		printf(CLEAR_TERMINAL DISABLE_WRAP RESET_FONT);
		printf("Noise values are RMS values in units of m/s2 and rad/s.\n");
		printf("Value in parenthesis are the max values recorded since the start.\n");
		printf("Values in " COLOR_RED "RED" RESET_FONT " are high ");
		printf("(>%0.2fm/s2 or >%0.2frad/s).\n", RED_BOUND_ACCL, RED_BOUND_GYRO);
		printf("Values in " COLOR_YLW "YELLOW" RESET_FONT " are okay ");
		printf("(>%0.2fm/s2 or >%0.2frad/s).\n", YLW_BOUND_ACCL, YLW_BOUND_GYRO);
		printf("Values in " COLOR_GRN "GREEN" RESET_FONT " are great!\n");
		printf("\n");
		printf(FONT_BOLD);
		printf(" Accl (X) | Accl (Y) | Accl (Z) |  Gyro (X)  |  Gyro (Y)  |  Gyro (Z)\n");
		printf(RESET_FONT);
		fflush(stdout);
	}
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// simple helper callback whenever data is ready
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	int i,j;
	static int filters_need_prefill = 1;
	static int first_print = 1;
	static int n_saved = 0; // current tally of saved values

	// validate that the data makes sense
	int n_received; // number of packets received this time around
	imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_received);

	// if there was an error OR no packets received, just return;
	if(data_array == NULL) return;
	if(n_received<=0) return;

	// if we are printing every packet, loop through each
	for(i=0;i<n_received;i++){

		// quit if it's time to exit
		if(!main_running) return;

		// on first run prefill our filters
		if(cutoff_freq>0 && filters_need_prefill){
			for(j=0;j<3;j++){
				rc_filter_prefill_inputs(&filters[j],  data_array[i].accl_ms2[j]);
				rc_filter_prefill_inputs(&filters[3+j],data_array[i].gyro_rad[j]);
			}
			filters_need_prefill = 0;
		}

		// run filter if enabled
		if(cutoff_freq>0){
			for(j=0;j<3;j++){
				vectors[j].d[n_saved]   = rc_filter_march(&filters[j],  data_array[i].accl_ms2[j]);
				vectors[3+j].d[n_saved] = rc_filter_march(&filters[3+j],data_array[i].gyro_rad[j]);
			}
		}
		// otherwise just add our values to the buffer
		else{
			for(j=0;j<3;j++){
				vectors[j].d[n_saved]   = data_array[i].accl_ms2[j];
				vectors[3+j].d[n_saved] = data_array[i].gyro_rad[j];
			}
		}

		// increment counter and check if we are finished
		n_saved++;
		if(n_saved>=n_samples){
			n_saved = 0; // reset counter
			double new[6];

			// calculate new values and max's
			for(j=0;j<6;j++){

				// input signal is already run through a high-pass filter, so
				// we assume the input signal to be 0-mean, this means we don't
				// need to explicitly calculate the mean when finding RMS
				double sum = 0.0;
				for(int k=0; k<vectors[j].len; k++){
					sum += vectors[j].d[k] * vectors[j].d[k];
				}
				new[j] = sqrt(sum/(vectors[j].len-1));

				// check if we hit the max, ignore the first sample as we wait
				// for the filter to converge. We prefill the filters so this
				// probably isn't necessary but it's an easy safeguard
				if(!first_print && new[j]>max[j]){
					max[j] = new[j];
				}
			}

			// print everything
			if(!newline) printf("\r");
			for(j=0;j<6;j++){

				// pick color and print new and max value
				if(j<3){
					
					if     (new[j]>=RED_BOUND_ACCL) printf(COLOR_RED);
					else if(new[j]>=YLW_BOUND_ACCL) printf(COLOR_YLW);
					else printf(COLOR_GRN);
					if(timeout && j == 0) printf("%s", "  ");
					if(timeout && j > 0) printf("%s","| ");
					printf("%4.2f"   , new[j]);
					if(timeout) printf("%s", "     ");
					

					if     (max[j]>=RED_BOUND_ACCL) printf(COLOR_RED);
					else if(max[j]>=YLW_BOUND_ACCL) printf(COLOR_YLW);
					else printf(COLOR_GRN);
					// print new, max, and reset color
					if(!timeout){
						printf("(%4.2f) " RESET_FONT, max[j]);
					}
				}
				else{
					if     (new[j]>=RED_BOUND_GYRO) printf(COLOR_RED);
					else if(new[j]>=YLW_BOUND_GYRO) printf(COLOR_YLW);
					else printf(COLOR_GRN);

					if(timeout) printf("%s", "| ");
					printf("%4.3f", new[j]);
					if(timeout) printf("%s", "     ");
					

					if     (max[j]>=RED_BOUND_GYRO) printf(COLOR_RED);
					else if(max[j]>=YLW_BOUND_GYRO) printf(COLOR_YLW);
					else printf(COLOR_GRN);
					// print new, max, and reset color
					if(!timeout){
						printf("(%4.3f) " RESET_FONT, max[j]);
					}
				}
				first_print = 0;
			}
			if(newline) printf("\n");
			fflush(stdout);
		}
	}
	
	// sleep for 1/10th of a second to ease the CPU load. We only print every
	// half second (ish) in normal use anyway.
	if(timeout && !first_print){
		usleep(1000000);
	}
	else {
		usleep(100000);
	}
	return;
}


int main(int argc, char* argv[])
{
	int i;
	time_t start_time, current_time;
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// create buffers and filters
	const double dt = 1.0 / SAMPLE_RATE;
	if(cutoff_freq>0){
		for(i=0;i<6;i++){
			filters[i] = rc_filter_empty();
			//rc_filter_first_order_highpass(&filters[i], dt, 1.0 / (2* M_PI * cutoff_freq));
			rc_filter_butterworth_highpass(&filters[i], 2, dt, 2.0 * M_PI * cutoff_freq);
		}
	}
	for(i=0;i<6;i++){
		vectors[i] = rc_vector_empty();
		rc_vector_alloc(&vectors[i],n_samples);
	}

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	if(!timeout){
		printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	}
	int ret = pipe_client_open(0, pipe_path, CLIENT_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
				IMU_RECOMMENDED_READ_BUF_SIZE);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}
	
	time(&start_time);
	// keep going until the  signal handler sets the running flag to 0
	while(main_running) {
		time(&current_time);
		if(timeout){
			if(difftime(current_time,start_time) >=2){
				break;
			}
		}
		usleep(500000);
	}
	// all done, signal pipe read threads to stop
	if(!timeout){
		printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	}
	pipe_client_close_all();

	return 0;
}
