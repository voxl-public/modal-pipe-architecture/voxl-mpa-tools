/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>
#include <sys/ioctl.h>
#include <signal.h>

#include <modal_pipe.h>
#include <voxl_cutils.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

// #define ENABLE_DEBUG_PIPE

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define gotoxy(x,y) printf("\033[%d;%dH", ((y)+1), ((x)+1))

#define CLIENT_NAME		"voxl-inspect-cam-ascii"
char pipe_name[64];

int term_width, term_height;
int new_term_size;

#define N_CHARS 10
static char map[N_CHARS + 1]=" .,:;ox%#@";

using namespace cv;

static void updateWinSize(){

    struct winsize winsz;

    ioctl(0, TIOCGWINSZ, &winsz);
    term_height = winsz.ws_row;
    term_width = winsz.ws_col;
    new_term_size = 1;
}

// ----------------------------------------------------------------------------
// Name   : sig_handler
// Usage  : Signal handler for SIGWINCH
// Return : None
// ----------------------------------------------------------------------------
static void sig_handler(int sig)
{
  if (SIGWINCH == sig) {
    updateWinSize();
  }

} // sig_handler

static void _print_usage(void) {
	printf("\n\
    \n\
    Print out camera data from Modal Pipe Architecture.\n\
    \n\
    Options are:\n\
    -h, --help       print this help message\n\
    \n\
    typical usage:\n\
    /# voxl-inspect-cam-ascii tracking\n\
    \n");
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",               no_argument,        0, 'h'},
		{0, 0, 0, 0}
	};


	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "h", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){

        if(pipe_expand_location_string(argv[i], pipe_name)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}

        // strcpy(pipe_name, argv[i]);
	}

    // make sure a pipe was given
    if(pipe_name[0] == 0){
        fprintf(stderr, "ERROR: Pipe name not specified\n");
        _print_usage();
        exit(-1);
    }

	return 0;
}

static char get_ascii_char(unsigned char pixelval){
    return map[pixelval*N_CHARS/256];
}

// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused))int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{

    if(pipe_client_bytes_in_pipe(ch)) return;

    //
    // VCU_silent(
    // system("resize");
    // );

    gotoxy(0,0);
    static float scale;
    static char *buffer;
    static int size = 0;

    if(new_term_size){
        double wscale, hscale;

        if(term_width >= meta.width) {
            wscale = 1.0;
        } else {
            wscale = ((float)term_width - 1) / meta.width;
        }

        if(term_height >= meta.height) {
            hscale = 1.0;
        } else {
            hscale = ((float)term_height - 5) / meta.height * 2;
        }

        scale = min(hscale, wscale);

        printf(CLEAR_TERMINAL);

        new_term_size = 0;
    }

    Mat frame_raw(meta.height, meta.width, CV_8UC1, frame);
    Mat frame_resized;
    resize(frame_raw, frame_resized, Size(), scale, scale/2);

    if(buffer == NULL) {
        buffer = (char *)malloc(((frame_resized.cols + 1) * frame_resized.rows) + 1);
        size = ((frame_resized.cols + 1) * frame_resized.rows) + 1;
    } else if(size != ((frame_resized.cols + 1) * frame_resized.rows) + 1) {
        buffer = (char *)realloc(buffer, ((frame_resized.cols + 1) * frame_resized.rows) + 1);
        size = ((frame_resized.cols + 1) * frame_resized.rows) + 1;
    }

    int counter = 0;

    for(int i=0; i<frame_resized.rows; i++) {
        for(int j=0; j<frame_resized.cols; j++) {
            buffer[counter++] = get_ascii_char(frame_resized.at<uchar>(i,j));
        }
        buffer[counter++] = '\n';
    }
    buffer[counter++] = 0;

    printf("%s\n", buffer);
    // printf("Size: %dx%d\n", frame_resized.cols, frame_resized.rows);
    fflush(stdout);

    #ifdef ENABLE_DEBUG_PIPE
    meta.width = frame_resized.cols;
    meta.stride = frame_resized.cols;
    meta.height = frame_resized.rows;
    meta.size_bytes = meta.width * meta.height;
    meta.format = IMAGE_FORMAT_RAW8;

    pipe_server_write_camera_frame(0, meta, frame_resized.data);
    #endif


	return;
}

int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

    // Capture SIGWINCH
    signal(SIGWINCH, sig_handler);
    updateWinSize();

    printf(CLEAR_TERMINAL);

    pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
    pipe_client_open(0, pipe_name, CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

    #ifdef ENABLE_DEBUG_PIPE
    pipe_info_t info;
    strcpy(info.name       , "ascii_downsample");
    strcpy(info.type       , "camera_image_metadata_t");
    strcpy(info.server_name, "voxl-inspect-cam-ascii");
    info.size_bytes = 64*1024*1024;

    pipe_server_create(0, info, 0);
    #endif

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

    // all done, signal pipe read threads to stop
    pipe_client_close_all();

	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);

	return 0;
}
